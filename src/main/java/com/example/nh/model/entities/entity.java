package com.example.nh.model.entities;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;

@Entity
@Data
@ToString(callSuper=true, includeFieldNames=true)
@NoArgsConstructor
@AllArgsConstructor
@Table(name="entity")
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public class entity {
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	@Column(name="Id",nullable=false)
	private int Id;
	@Column(name="Label",nullable=false)
	private String Label;
	@Column(name="Description",nullable=false)
	private String Description;
	@Column(name="Identification",nullable=false)
	private String Identification;
	
	
	
}
