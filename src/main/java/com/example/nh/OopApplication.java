package com.example.nh;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;

import com.example.nh.model.entities.Person;
import com.example.nh.model.entities.entity;
import com.example.nh.repository.entity.EntityR;
import com.example.nh.repository.entity.PersonR;

@SpringBootApplication(exclude = {SecurityAutoConfiguration.class })
public class OopApplication  implements CommandLineRunner{
	
	@Autowired
	private EntityR rr;
	@Autowired
	private PersonR pr;
	public static void main(String[] args) {
	  SpringApplication.run(OopApplication.class, args);
		
		}
	
	public void run(String... args) throws Exception {
		
	  
		Person p2=new Person();
		p2.setId(2);
		p2.setLabel("aaa");
		p2.setIdentification("aaaa");
		p2.setDescription("aaaa");
		p2.setNationality("aaaa");
		p2.setPosition("aaaaaa");
		
		pr.save(p2);
		pr.findAll()
	    .forEach(System.out::println);

	}

}
